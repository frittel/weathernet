# Multilayer Perceptron to Predict International Airline Passengers (t+1, given t)
import math

import matplotlib.pyplot as plt
import numpy
import pandas
from keras.layers import Dense
from keras.models import Sequential


# fix random seed for reproducibility


def run_keras_predictor(lookback, diff):
    numpy.random.seed(7)
    # load the dataset
    dataframe = pandas.read_csv('basel_data_short.csv', usecols=[5], engine='python', sep=";")
    dataset = dataframe.values
    dataset = dataset.astype('float32')
    # split into train and test sets
    train_size = int(len(dataset) * 0.67)
    test_size = len(dataset) - train_size
    train, test = dataset[0:train_size, :], dataset[train_size:len(dataset), :]
    print(len(train), len(test))
    # convert an array of values into a dataset matrix

    # reshape into X=t and Y=t+1
    trainX, trainY = create_dataset(train, lookback, diff)
    testX, testY = create_dataset(test, lookback, diff)
    # create and fit Multilayer Perceptron model
    model = Sequential()
    model.add(Dense(8, input_dim=lookback, activation='relu'))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adamax')
    model.fit(trainX, trainY, epochs=1, batch_size=1, verbose=0)
    # Estimate model performance
    trainScore = model.evaluate(trainX, trainY, verbose=0)
    print('Train Score: %.2f MSE (%.2f RMSE)' % (trainScore, math.sqrt(trainScore)))
    testScore = model.evaluate(testX, testY, verbose=0)
    print('Test Score: %.2f MSE (%.2f RMSE)' % (testScore, math.sqrt(testScore)))
    # generate predictions for training
    trainPredict = model.predict(trainX)
    testPredict = model.predict(testX)
    # shift train predictions for plotting
    trainPredictPlot = numpy.empty_like(dataset)
    trainPredictPlot[:, :] = numpy.nan
    trainPredictPlot[lookback:len(trainPredict) + lookback, :] = trainPredict
    # shift test predictions for plotting
    testPredictPlot = numpy.empty_like(dataset)
    testPredictPlot[:, :] = numpy.nan
    testPredictPlot[len(trainPredict) + (lookback * 2) + 1 + diff :len(dataset) - 1 - diff, :] = testPredict
    # plot baseline and predictions
    plt.plot(dataset)
    plt.plot(trainPredictPlot[diff-4:])
    plt.plot(testPredictPlot[diff-4:])
    plt.legend(["dataset", "training_pred", "test_pred"])
    plt.show()


def create_dataset(dataset, look_back=1, diff=0):
    dataX, dataY = [], []
    for i in range(len(dataset) - look_back - 1 - diff):
        a = dataset[i:(i + look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back + diff, 0])
    return numpy.array(dataX), numpy.array(dataY)


if __name__ == '__main__':
    run_keras_predictor(8, 12)
