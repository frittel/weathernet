clc
clear
p=1;
off = 48;

M = csvread('basel.csv');
zeilen=size(M)/5;
Input_raw(:,1)=M(:,6);

for i = 1:zeilen
    for tempp = 1:p
    Input(i,tempp)=Input_raw(i+tempp-1,1);
    
    end
end

for i = 1:zeilen
    for tempp = 1:p
    Target(i,1)=Input_raw(i+off+p-1,1);
    
    end
end


zeit=40000;
for i=zeit:zeit+1000
 for tempp = 1:p
    lookback(1,tempp)=Input_raw(i+tempp-1,1);
 end
    Temp(i-off-1,1)=TempFunction_ts(lookback,num2cell(Input_raw(i-off:i,1)'));
end

figure(1)
plot(Temp)
hold on
plot(Input_raw)
legend