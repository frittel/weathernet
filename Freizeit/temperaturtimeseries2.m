clc
clear
p=1;
off = 48;

M = csvread('basel.csv');
zeilen=size(M)/5;
Input_raw(:,1)=M(:,6);

for i = 1:zeilen
    Input(i,1)=Input_raw(i,1);
    
end

for i = 1:zeilen
    Target(i,1)=Input_raw(i+off,1);
    
end


zeit=40000;
for i=zeit:zeit+1000+off*2
    Temp(i-off,1)=Input_raw(i-off,1);
end

Xi = zeros(2,off);

for i=zeit:zeit+1000
    lookback(1,i)=Input_raw(i-1,1);
    lookback(2,i)=Temp(i+off,1);
end
Xi(1,1:off) = Input_raw(i-off:i-1,1)';
Xi(2,1:off) = Temp(i:i+off-1,1)';


output=TempFunction_ts2(lookback,num2cell(Xi));


figure(1)
plot(output)
hold on
plot(Input_raw)
legend